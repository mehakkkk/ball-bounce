var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var c1;
var c2;
var p = new Circle.Point(100, 530);
var p1 = new Circle.Point(300, 530);
var line = new Circle.Line(p, p1, context);
var count;
function start1() {
    count = 0;
    var points1 = new Circle.Init(200, 200, 30);
    c2 = new Circle.Circledraw(points1, "grey", context);
    line.draw();
    anim();
}
function anim() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    c2.draw();
    line.draw();
    c2.update();
    window.requestAnimationFrame(anim);
}
//# sourceMappingURL=circapp.js.map