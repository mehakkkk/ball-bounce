var Circle;
(function (Circle) {
    class Init {
        constructor(x, y, radius) {
            this.x = x;
            this.y = y;
            this.radius = radius;
        }
    }
    Circle.Init = Init;
    //point class
    class Point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Circle.Point = Point;
    //circle class
    class Circledraw {
        constructor(pxyr, color, context) {
            this.pxyr = pxyr;
            this.context = context;
            this.ang = 0;
            this.color = color;
            this.fac = 0.8;
            this.g = 0.1;
            this.speedX = 0;
            this.speedY = 0;
            this.gravity = 0.1;
            this.gravitySpeed = 0;
            this.bounce = 0.6;
            this.canvasheight = 600;
            this.height = 100;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.pxyr.x, this.pxyr.y, this.pxyr.radius, 0, 2 * Math.PI, true);
            this.context.lineWidth = 1;
            //this.context.fill();
            this.context.stroke();
        }
        update() {
            this.gravitySpeed += this.gravity;
            this.pxyr.x += this.speedX;
            this.pxyr.y += this.speedY + this.gravitySpeed;
            this.rockbottom = this.canvasheight - this.height;
            //console.log(this.pxyr.y)
            if (this.pxyr.y > this.rockbottom) {
                this.pxyr.y = this.rockbottom;
                this.gravitySpeed = -(this.gravitySpeed * this.bounce);
            }
        }
    }
    Circle.Circledraw = Circledraw;
    class Line {
        constructor(p1, p2, context) {
            this.p1 = p1;
            this.p2 = p2;
            this.context = context;
        }
        draw() {
            this.context.beginPath();
            this.context.moveTo(this.p1.x, this.p1.y);
            this.context.lineTo(this.p2.x, this.p2.y);
            this.context.lineWidth = 3;
            this.context.stroke();
        }
    }
    Circle.Line = Line;
})(Circle || (Circle = {}));
//# sourceMappingURL=circgeom.js.map