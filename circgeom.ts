namespace Circle
{
    export class Init
    {
        public radius:number;
        public x: number;
        public y: number;
        constructor(x:number,y:number,radius:number)
        {
            this.x=x;
            this.y=y;
            this.radius=radius;

        }

    }

    //point class
    export class Point
    {
        public x:number;
        public y:number;
        constructor(x:number,y:number){
            this.x = x;
            this.y = y;
        }
    }


    //circle class
   export class Circledraw
    {
        public pxyr:Init;
        public context:CanvasRenderingContext2D;
        public ang:number;
        public length:number;
        public color:string;
        private g:number; // gravity
        private fac:number; // velocity reduction factor per bounce
        private speedX:number;
        private speedY:number;
        private gravity:number;
        private gravitySpeed:number;
        private bounce:number;
        private rockbottom:number;
        private canvasheight:number;
        private height:number;
        constructor( pxyr:Init,color:string,context:CanvasRenderingContext2D)
        {
            this.pxyr=pxyr;
            this.context=context;
            this.ang=0;
            this.color = color;
            this.fac=0.8;
            this.g=0.1;
           
            this.speedX = 0;
            this.speedY = 0;
            this.gravity = 0.1;
            this.gravitySpeed = 0;
            this.bounce = 0.6;
            this.canvasheight=600;
            this.height=100;
        }
  
        draw()
        {

            this.context.beginPath()
            this.context.arc(this.pxyr.x,this.pxyr.y,this.pxyr.radius,0,2*Math.PI,true)
            this.context.lineWidth=1;
            //this.context.fill();
            this.context.stroke();
        }
        
  update() {
    this.gravitySpeed += this.gravity;
    this.pxyr.x += this.speedX;
    this.pxyr.y += this.speedY + this.gravitySpeed;
    
     this.rockbottom = this.canvasheight-this.height;
     //console.log(this.pxyr.y)
    if (this.pxyr.y > this.rockbottom) {
      this.pxyr.y = this.rockbottom;
      this.gravitySpeed = -(this.gravitySpeed * this.bounce);
    }
  }
        

    }
    export class Line
    {
        public p1: Point;
        public p2: Point;
        public context: CanvasRenderingContext2D;
        
        constructor(p1: Point,p2: Point,context: CanvasRenderingContext2D)
        {
            this.p1=p1;
            this.p2=p2;
            this.context = context;
           
        }
        draw()
        {
            this.context.beginPath();
            this.context.moveTo(this.p1.x,this.p1.y);
            this.context.lineTo(this.p2.x,this.p2.y);
            this.context.lineWidth=3;
            this.context.stroke();
        }
        
    }
}